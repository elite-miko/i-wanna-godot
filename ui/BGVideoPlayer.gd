extends CanvasLayer

onready var player:VideoPlayer = $VideoPlayer

func play(stream:VideoStream = null, stream_length_secs:float = 100.0) -> void:
	if stream:
		player.stream = stream
	$ProgressBar.max_value = stream_length_secs
	
	player.play()

func stop() -> void:
	player.stop()

func set_video_pos(sec:float) -> void:
	player.stream_position = sec

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	$ProgressBar.value = player.stream_position
