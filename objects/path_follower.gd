extends PathFollow2D

export var speed:float = 0.2

#func _physics_process(delta):
#	unit_offset += speed * delta

func _ready():
	$AnimationPlayer.playback_speed = speed
	$AnimationPlayer.play("loopit")
