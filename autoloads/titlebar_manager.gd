extends Node

var cur_seconds:float = 0
var deaths:int = 0
var room_description:String = ""

func _ready():
	connect_to_new_scene()

func scene_exit():
	call_deferred("connect_to_new_scene")

func connect_to_new_scene() -> void:
	var curr_scene:Node = get_tree().current_scene
	curr_scene.connect("tree_exiting", self, "scene_exit")
	
	for player in get_tree().get_nodes_in_group("players"):
		player.connect("died", self, "on_player_death")
	
	if "description" in curr_scene:
		room_description = curr_scene.description
	else:
		room_description = "%s: %s" % [
			ProjectSettings.get("application/config/name"), 
			get_tree().current_scene.name
		]

func _process(delta):
	cur_seconds += delta
	var time = get_time_dict()
	OS.set_window_title("%s | %s | %s deaths | (%s PFPS, %s DFPS)" % [
		room_description,
		"%02d:%02d:%02d" % [time.hours, time.minutes, time.seconds],
		deaths,
		Engine.iterations_per_second,
		Engine.get_frames_per_second()
	])

func get_time_dict():
	var hours:int = int(cur_seconds / 3600.0)
	var remainder:int = int(cur_seconds - hours * 3600.0)
	var minutes:int = int(remainder/60.0)
	remainder = remainder - minutes * 60
	var seconds:int = remainder
	
	return {
		"hours": hours,
		"minutes": minutes,
		"seconds": seconds
	}

func on_player_death():
	deaths += 1
