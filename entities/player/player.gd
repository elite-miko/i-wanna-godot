extends KinematicBody2D
class_name Player

signal died()

export var player_num:int = 1
export var player_name:String = "Kid"

# all of the original values are at 50 frames per second
const framecontrol:int = Engine.iterations_per_second

export var speed:float = 3 * framecontrol
export var gravity:float = 0.4 * framecontrol
export var gravity_direction:Vector2 = Vector2(0, 1)

var max_speed:Vector2 = Vector2(3 * framecontrol, 9 * framecontrol)

export var velocity:Vector2 = Vector2(0,0)

onready var anim_player:AnimationPlayer = $AnimationPlayer

export var jump_count:int = 0
export var jump_max:int = 2

export var max_slope:float = 0.785398
export var is_frozen:bool = false setget set_frozen
export var is_infinite_jump:bool = false
var is_upside_down:bool = false

onready var bullet:PackedScene = preload("bullet/bullet.tscn")
export var bullet_max:int = 4
export var bullet_interval:float = 0.1
var current_bullets:int = 0
export var has_projectile_dysfunction:bool = false

onready var blood_emitter:PackedScene = preload("blood/blood_emitter.tscn")
onready var player_head:PackedScene = preload("head_roll/player_head.tscn")

var states:Dictionary = {
	"idle": preload("state_idle.gd").new(),
	"running": preload("state_run.gd").new(),
	"jump": preload("state_jump.gd").new(),
	"fall": preload("state_fall.gd").new(),
}
var current_state:String = "idle"

func _ready():
	set_frozen(is_frozen)

func _physics_process(delta):
	if current_state in states:
		states[current_state].update(self, delta)
	else:
		printerr("Invalid state: ", current_state)

func handle_flip(x:float):
	if x < 0: #facing right
		$Sprite.scale.x = -1
		if is_upside_down:
			$Sprite.scale.x = 1
	elif x > 0: #facing left
		$Sprite.scale.x = 1
		if is_upside_down:
			$Sprite.scale.x = -1
	
func flip_vertical(upright:bool = true):
	if upright:
		$Sprite.rotation_degrees = 0
		if is_upside_down:
			gravity_direction.y *= -1
			position.y += $Shape.shape.extents.y*2
			refresh_jump()
		is_upside_down = false
	else:
		$Sprite.rotation_degrees = 180
		if not is_upside_down:
			gravity_direction.y *= -1
			position.y -= $Shape.shape.extents.y*2
			refresh_jump()
		is_upside_down = true

func change_state(new_state:String):
	if new_state in states:
		states[current_state].exit(self)
		current_state = new_state
		states[new_state].enter(self)
	else:
		printerr("Tried to enter a new state but the state doesn't exist: ", new_state)


func apply_gravity():
	velocity += gravity_direction.normalized() * gravity

func get_movement() -> Vector2:
	var desired_movement = Vector2(0,0)
	if Input.is_action_pressed(get_input_left()):
		desired_movement.x = -1
	if Input.is_action_pressed(get_input_right()):
		desired_movement.x = 1
	
	return desired_movement.normalized()

func set_frozen(b:bool) -> void:
	is_frozen = b
	set_physics_process(!b)

func can_jump() -> bool:
	if is_on_floor():
		jump_count = 0
	if is_infinite_jump:
		refresh_jump()
	return (jump_count < jump_max) 

func get_input_jump() -> String:
	return "player_%s_jump" % player_num

func get_input_left() -> String:
	return "player_%s_left" % player_num

func get_input_right() -> String:
	return "player_%s_right" % player_num
	
func get_input_shoot() -> String:
	return "player_%s_shoot" % player_num

func get_input_reset() -> String:
	return "player_%s_reset" % player_num

func get_input_suicide() -> String:
	return "player_%s_suicide" % player_num

func cap_vertical_speed():
	velocity.y = clamp(velocity.y, -max_speed.y, max_speed.y)

func cap_horizontal_speed():
	velocity.x = clamp(velocity.x, -max_speed.x, max_speed.x)

func cap_speed():
	cap_horizontal_speed()
	cap_vertical_speed()

func shoot():
	if current_bullets < bullet_max and $ShootTimer.is_stopped():
		$ShootTimer.start(bullet_interval)
		var b = bullet.instance()
		b.player = self
		b.direction = Vector2(1, 0).normalized()
		if has_projectile_dysfunction:
			b.random_rotation = true
		current_bullets += 1
		b.connect("bullet_destroyed", self, "bullet_destroyed")
		get_tree().current_scene.add_child(b)
		b.global_position = $Sprite/PosBullet.global_position
		b.global_rotation = $Sprite.global_rotation

func bullet_destroyed():
	current_bullets -= 1

func kill():
	var emitter = blood_emitter.instance()
	get_tree().current_scene.add_child(emitter)
	emitter.global_position = global_position
	var head = player_head.instance()
	get_tree().current_scene.add_child(head)
	head.global_position = $Sprite/PosHead.global_position
	play_sound("death")
	set_physics_process(false)
	visible = false
	emit_signal("died")

func check_for_death() -> bool:
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.get_collision_layer_bit(3): #killer
			return true
	
	return false

func is_touching_platform() -> bool:
	var space := get_world_2d().direct_space_state
	var params := Physics2DShapeQueryParameters.new()
	params.set_shape($Shape.shape)
	params.transform = transform
	params.exclude = [self]
	params.collide_with_areas = true
	var objs := space.intersect_shape(params)
	
	for obj in objs:
		if obj["collider"].is_in_group("platforms"):
			return true
	
	return false
	
func is_on_platform() -> bool:
	var space := get_world_2d().direct_space_state
	var objs := space.intersect_point(global_position, 32, [self], 0x7FFFFFFF, true, true)
	
	for obj in objs:
		if obj["collider"].is_in_group("platforms"):
			return true
	
	return false

func get_platform_on() -> Node2D:
	var space := get_world_2d().direct_space_state
	var objs := space.intersect_point(global_position, 32, [self], 0x7FFFFFFF, true, true)
	
	for obj in objs:
		if obj["collider"].is_in_group("platforms"):
			return obj["collider"]
	
	return null

func play_animation(anim:String) -> void:
	if anim_player.has_animation(anim):
		anim_player.play(anim)
	else:
		printerr("Player could not find animation", anim)
	pass

func play_sound(sound:String) -> void:
	if $ResourcePreloader.has_resource(sound):
		$SFX.stream = $ResourcePreloader.get_resource(sound)
		$SFX.play()
	else:
		printerr("Tried to play sound but couldn't find it: ", sound)

func refresh_jump():
	jump_count = 1
