extends KinematicBody2D
class_name PlayerBullet

var player
export var speed = 16.0
export var direction = Vector2(0, 0)
export var random_rotation:bool = false

signal bullet_destroyed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if random_rotation: #projectile dysfunction
		rotation += rand_range(-10, 10) * delta
	var collision = move_and_collide(direction.rotated(rotation) * speed)
	if collision:
		if collision.collider.has_method("get_shot"):
			collision.collider.get_shot(player)
		destroy()

func _on_Kill_timeout():
	destroy()

func destroy():
	emit_signal("bullet_destroyed")
	hide()
	set_physics_process(false)
	if $AudioStreamPlayer2D.playing:
		yield($AudioStreamPlayer2D,"finished")
	queue_free()
