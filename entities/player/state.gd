extends Resource
class_name PlayerState

func enter(player:KinematicBody2D) -> void:
	pass

func exit(player:KinematicBody2D) -> void:
	pass

func update(player:KinematicBody2D, delta:float) -> void:
	pass

func handle_normal_movement(player:KinematicBody2D):
	player.velocity.x = player.max_speed.x

func handle_slippery_blocks(player:KinematicBody2D, movement:Vector2) -> void:
#	var found_slip:bool = false
#
#	for i in player.get_slide_count():
#		var collision = player.get_slide_collision(i)
#		if "physics_material_override" in collision.collider:
#			if movement.x < 0:
#				player.velocity.x += collision.collider.physics_material_override.friction
#			else:
#				player.velocity.x += collision.collider.physics_material_override.friction
#			player.cap_horizontal_speed()
#			found_slip = true
#			break
#
#	if not found_slip:
#		# if the player is not touching a slippery block
#		# immediately set the horizontal_speed to the maximum amount
	handle_normal_movement(player)

func handle_moving_blocks(player:KinematicBody2D):
	if player.is_on_floor():
		player.velocity += player.get_floor_velocity() / 50.0

func object_at(x:float, y:float):
	pass
