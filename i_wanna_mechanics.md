# Understanding this guide

This guide exists to try relay the mechanics of an "I wanna be the guy" fangame so that it's easier to make new engines/games derived from it.

Where possible I will try to include basic python code with comments to help explain mechanics.

## Frame Timings

Every I wanna fangame runs at 50 FPS, so everything frame-related in this guide is based around that amount. I tried to include a simple graph that shows where each action lies in a particular animation. Below are some example graphs as well as a breakdown of what they mean.

### Legend:
```toml
A-Z,0-9 # denotes an action, frame counter, whatever makes sense in context
- # denotes to hold the previous frame
. # denotes to terminate the action (releases on this frame, wipes data, whatever)
| | # pipes surrounding means it loops
```

The previous action stops when a new one is defined and wasn't already stopped due to a period symbol.

### Simple Hold Example:
```
|A--B--C--D--E--|
```

This graph can be read as follows:
1. Action A starts. 
1. Action A gets held
1. Action A gets held
1. Action B starts
1. Action B gets held
1. Action B gets held
1. Action C starts
1. (etc)
1. Entire action repeats indefinitely

### Mixing Holds and Releases
```
A-.B-.A...B--.
```

This graph can be read as follows:
1. Action A starts
1. Action A gets held
1. Action A stops
1. Action B starts
1. Action B gets held
1. Action B stops
1. Action A starts
1. Action A stops
1. Nothing
1. Nothing
1. Action B starts
1. Action B gets held
1. Action B gets held
1. Action B stops

# Overall Game

* FPS: 50
* Window Dimensions: Variable
* Title bar typically shows the elapsed time in the save file, along with how many deaths. Also usually shows the name of the game.

# The Kid

* The main player. Typically has a gun.
* Arrows = Movement
    * Up/Down for entering doors if applicable, or up/down ladders
    * Right takes precedence over Left if both are pressed
* Shift = jump
* Z = shoots gun
* S = activate save point when touching (if enabled)
* R = return from death back to last save point
* Q = force kill
* P = pause
* M = toggle mute music
* Wears a bow on the lowest difficulty setting

## Physics Values

```python
jump = 8.5 # the 1st jump velocity
jump2 = 7 # the 2nd jump velocity
gravity = 0.4

max_horizontal_speed = 3
max_vertical_speed = 9

can_double_jump = True

image_speed = 0.2 # the default animation speed
# a value of 1 is one frame per one frame.
# a value of 0.2 would therefore mean one frame of animation per five frames
```

## States

The player can look right or left. All of the states below assume the player is looking to the right.

Information can and will be duplicated between states as they are designed to be used in a StateMachine, and are not aware of the mechanics of the others. It is possible to move the shared logic to the player character instead of the states if desired. However for clarity everything will be duplicated in this guide if it applies.

### Idle
* Animation Info
    * 4 actual animation frames
    * `image_speed = 0.2` (`|0----1----2----3----|`)
    * 20 total animation frames

#### Runtime Logic
```python
def update(delta):
    handle_slippery_blocks()
    handle_moving_blocks()
    cap_maximum_vertical_speed()
    handle_input()

def handle_slippery_blocks():
    if touching_slippery_block:
        # if the player is on a slippery block
        # move the player towards 0 horizontal speed using the slip amount as "friction"
        # if the player reaches 0 or lower, set it to 0
        horizontal_speed -= slippery_block.slip_amount
        if horizontal_speed <= 0:
            horizontal_speed = 0
    else:
        # if the player is not touching a slippery block
        # immediately set the horizontal speed to 0
        horizontal_speed = 0

def handle_moving_blocks():
    if standing_on_moving_block:
        # If there is a block that is moving underneath the player,
        # add the horizontal speed of the block to the player
        horizontal_speed += moving_block.horizontal_speed

def cap_maximum_vertical_speed():
    if abs(vertical_speed) > max_vertical_speed:
        # if our vertical speed is too fast, cap it to max speed
        vertical_speed = max_vertical_speed

def handle_input():
    if not is_frozen:
        # if the player is not currently frozen (in a cutscene, disabled, etc)
        # handle all other input and call their resulting functions
        if is_shoot_pressed():
            shoot()
        if is_jump_pressed():
            jump()
        if is_jump_released():
            un_jump()
        if is_suicide_pressed():
            kill_player()
```

### Run
Activates when the player indicates that they want to move left/right, and is on the ground. Additionally, the player needs to not be touching a "vine".

* Animation info
    * 5 actual animation frames
    * `image_speed = 0.5` (`|0-1-2-3-4-|`)
    * 10 total animation frames

#### Runtime Logic
```python
def update(delta):
    handle_slippery_blocks()
    handle_moving_blocks()
    cap_maximum_vertical_speed()
    handle_input()
    handle_slopes()
    check_for_death()

def handle_slippery_blocks():
    if touching_slippery_block:
        # if the player is on a slippery block
        # increase the horizontal speed by the slip amount of the block
        horizontal_speed += slippery_block.slip_amount

        if abs(horizontal_speed) > max_horizontal_speed:
            # cap the horizontal speed to the maximum horizontal speed
            horizontal_speed = max_horizontal_speed
    else:
        # if the player is not touching a slippery block
        # immediately set the horizontal_speed to the maximum amount
        horizontal_speed = max_horizontal_speed

def handle_moving_blocks():
    if standing_on_moving_block:
        # If there is a block that is moving underneath the player,
        # add the horizontal speed of the block to the player
        horizontal_speed += moving_block.horizontal_speed

def cap_maximum_vertical_speed():
    if abs(vertical_speed) > max_vertical_speed:
        # if our vertical speed is too fast, cap it to max speed
        vertical_speed = max_vertical_speed

def handle_input():
    if not is_frozen:
        # if the player is not currently frozen (in a cutscene, disabled, etc)
        # handle all other input and call their resulting functions
        if is_shoot_pressed():
            shoot()
        if is_jump_pressed():
            jump()
        if is_jump_released():
            un_jump()
        if is_suicide_pressed():
            kill_player()

def handle_slopes():
    if not room_has_objects(ObjectSlope):
        # if the room doesn't have any slope objects, quickly return
        return
    if horizontal_speed == 0:
        # if we're not moving horizontally, quickly return
        return

    # sets how high/low the player can go to snap onto a slope, 
    # this can be increased to make the player able to run over steeper slopes 
    # (ie setting it to abs(hspeed)*2 allows the player to run over slopes twice as steep)
    move_limit = abs(horizontal_speed)

    # check if the player is currently falling onto a slope
    if object_at(x + horizontal_speed, y + horizontal_speed + gravity) == ObjectSlope and vertical_speed+gravity > 0 and is_not_on_block:
        # if a "slope" object would be underneath us next frame
        # and vertical_speed + gravity is pushing us downwards
        # and we're not on a block

        #store our previous position and speed in case we reject the movement
        previous_pos = Vector2(x, y)
        previous_speed = Vector2(horizontal_speed, vertical_speed)

        # set the vertical and horizontal speeds to try and check for the slope
        vertical_speed += gravity
        x += horizontal_speed
        horizontal_speed = 0

        if place_free(x, y + vertical_speed):
            # if there's nothing underneath the player at the desired speed,
            # set their Y coordinate to be that desired speed
            y += vertical_speed
        else:
            # otherwise if the player would intersect with the slope at that vertical velocity
            # move the player down until it collides
            move_player_until_collision(down, vertical_speed)
            vertical_speed = 0
        
        if not place_free(x, y+1) and place_free(x, y):
            # if the player has a block directly below them, and is not intersecting
            # allow them to jump again
            # and say they're on a block
            can_double_jump = True
            is_on_block = True
        else:
            # could not find a proper location to snap to a slope onto
            # reset everything to the original values
            x = previous_pos.x
            y = previous_pos.y
            horizontal_speed = previous_speed.x
            vertical_speed = pervious_speed.y
        
    # otherwise check if the player is moving down a slope
    else if is_on_block:
        is_on_slope = (object_at(x, y+1) == ObjectSlope)


        horizontal_test = horizontal_speed

        while True:
            slope_y = 0

            # check how far to move down
            while (object_at(x + horizontal_test, y - slope_y + 1) == ObjectSlope or (is_on_slope and object_at(x + horizontal_test, y - slope_y + 1) == ObjectBlock)) and slope_y > -floor(move_limit*horizontal_test/horizontal_speed):
                # check if there is a slope in the spot we're going, and find the top of it
                # if there's a block in the spot we're going, find the top of it as well
                # as long as the amount of slope_y is within range

                # decrement slope_y each loop to find the spot
                slope_y -= 1
            
            #check if we actually need to move down
            if object_at(x+horizontal_test, y-slope_y+1) == ObjectSlope or (is_on_slope and object_at(x + horizontal_test, y - slope_y + 1) == ObjectBlock)):
                if slope_y != 0 and not object_at(x+horizontal_test, y-slope_y) == ObjectBlock:
                    # if we've found something to snap to
                    # and the place directly above that spot isn't a block
                    # set the player to be directly above the spot and end the loop
                    y -= slope_y
                    x += horizontal_test
                    horizontal_speed = 0
                    break
                else:
                    # we've found a spot to move to, but it's likely blocked by a block
                    # so change the amount of horizontal to check and repeat the loop
                    # if the amount of horizontal to check is 0 (no movement), break the loop
                    # otherwise continue
                    if horizontal_test > 0:
                        horizontal_test -= 1
                        if horizontal_test <= 0:
                            break
                    else if horizontal_test < 0:
                        horizontal_test += 1
                        if horizontal_test >= 0:
                            break
                    else: # == 0
                        break
            else:
                # could not find a valid spot, rejecting and exiting slope loop
                break
            
    # check if player is moving up a slope
    if object_at(x+horizontal_speed, y) == ObjectSlope:

        horizontal_test = horizontal_speed

        while True:
            slope_y = 0

            #check how far to move up
            while object_at(x + horizontal_test, y - slope_y) == ObjectSlope and slope_y < floor(move_limit*horizontal_test/horizontal_speed):
                slope_y += 1
            
            # check if we need to move up
            if place_free(x + horizontal_test, y - slope_y):
                y -= slope_y
                x += horizontal_test
                horizontal_speed = 0
                break
            else:
                if horizontal_test > 0:
                    horizontal_test -= 1
                    if horizontal_test <= 0:
                        break
                else if horizontal_test < 0:
                    horizontal_test += 1
                    if horizontal_test >= 0:
                        break
                else: # == 0
                    break

def check_for_death():
    # if the room is set to kill the player upon reaching the edge boundaries
    if room.edge_death:
        # check if the player is outside of the room bounds
        if x < 0 or x > room_width:
            kill_player()
        if y < 0 or y > room_height:
            kill_player()
     
    # check if the player is on top of an object that kills the player
    if object_at(x, y) == ObjectPlayerKiller:   
        kill_player()

def shoot():
    if instance_count(ObjectBullet) < 4:
        ObjectBullet.new(x, y)
        play_sound("sound_shoot")

def jump():
    if object_at(x, y+1) in [ObjectBlock, ObjectWater] or is_on_platform:
        vertical_speed = -jump_power
        can_double_jump = True
        play_sound("sound_jump")
    else if can_infinite_jump or can_double_jump or object_at(x, y+1) in [ObjectWater2]:
        vertical_speed = -doublejump_power
        play_sound("sound_doublejump")

        if not object_at(x, y+1) == ObjectWater3:
            can_double_jump = False
        else:
            can_double_jump = True

def un_jump():
    if vertical_speed < 0:
        vertical *= 0.45

def kill_player():
    if global.no_death:
        # if killing the player is turned off, quickly return
        return
    if global.game_started:
        play_sound("sound_death")
        play_music("death_music")

        create_particle(x, y, ObjectBloodEmitter)
        free()

        ObjectGameOver.new(0, 0)

        global.death_count += 1

        global.save_game(save_the_location: false)
    else:
        # difficulty select room, or some other place with control over the player
        free()
        room_restart()
```

### Jumping

* 2 frames of animation
* Can jump two times

### Falling

* 2 frames of animation

### Vine Slide

* Animation info
    * 2 actual animation frames
    * `image_speed = 2` (`|0-1-|`)
    * 4 total animation frames

#### Runtime Logic
```python
def update():
    if is_on_vine():
        # if the player is currently touching a vine block and sliding on it
        vertical_speed = 2

        if on_left_vine and is_right_pressed() or on_right_vine and is_left_pressed():
            if is_jump_pressed():
                # if jump is held down when moving off of the fine
                # set the speed values and change to the jumping state
                horizontal_speed = 15
                vertical_speed = -9
                change_state("jumping")
            else:
                # if we do not have jump pressed while moving off of the vine
                # set the horizontal speed and change to the falling state
                horizontal_speed = 3
                change_state("falling")
                
    else:
        # if we're no longer on a vine
        # return to some other state, or pop this state from the stack
        change_state("falling")
```



# Objects

Information about some various common objects that appear in the game

## Delicious Fruit

* Could be an apple, could be a cherry. Nobody really knows.
* 2 frames of animation
* Kills the kid on contact

## KILL BLOCK

* A square block that kills the kid on contact

## Spike

* A triangular block that kills the kid on contact

## Ice Floor

* Very slippery

## Save

* Activated by Save button
* Activated by Shooting
* Activated by Touch

## Vine

* Left/Right variants

## Button

* Activates an arbitrary trigger somewhere to do something.
* Shoot the button to activate it

## Invisible Trigger

* Activates a trap or some other change in the level geometry

## Normal Water

* Resets the kid's jump count so that he can jump once out of water, and once in the air again.

## Jank Water

* Does not reset the kid's jump count, unlike normal water